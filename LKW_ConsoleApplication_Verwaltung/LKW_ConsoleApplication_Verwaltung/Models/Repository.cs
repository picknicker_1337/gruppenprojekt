﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKW_ConsoleApplication_Verwaltung.Models
{
    class Repository
    {
        // Reader für Dateien
        public StreamReader LkwTxtReader { get; set; }

        public StreamReader PalettenTxtReader { get; set; }

        // Listen für Modelle
        List<LKW> lkwList = new List<LKW>();

        List<Palette> palettenList = new List<Palette>();

        // Konstruktor
        public Repository(string lkwPath, string palettenPath)
        {
            LkwTxtReader = new StreamReader(lkwPath);
            PalettenTxtReader = new StreamReader(palettenPath);
        }

        // Textdateien auslesen
        public void ReadTxt()
        {
            if (LkwTxtReader != null)
            {
                using (LkwTxtReader)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("LKW auslesen..");
                    Console.ForegroundColor = ConsoleColor.Yellow;

                    string tempLine = LkwTxtReader.ReadLine();

                    Console.WriteLine(tempLine);

                    while (!String.IsNullOrEmpty(tempLine = LkwTxtReader.ReadLine()))
                    {
                        string[] splittedTempLine = tempLine.Split(';');
                        LKW einLKW = new LKW(splittedTempLine[0], Convert.ToInt32(splittedTempLine[1]), Convert.ToInt32(splittedTempLine[2]));
                        lkwList.Add(einLKW);
                        Console.WriteLine(tempLine);
                    }

                    Console.ResetColor();
                }
            }
            if (PalettenTxtReader != null)
            {
                using (PalettenTxtReader)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Paletten auslesen..");
                    Console.ForegroundColor = ConsoleColor.Yellow;

                    string tempLine = PalettenTxtReader.ReadLine();

                    while (!String.IsNullOrEmpty(tempLine = PalettenTxtReader.ReadLine()))
                    {
                        string[] splittedTempLine = tempLine.Split(';');
                        Palette einePalette = new Palette(splittedTempLine[0], Convert.ToInt32(splittedTempLine[1]));
                        palettenList.Add(einePalette);
                        Console.WriteLine(tempLine);
                    }

                    Console.ResetColor();
                }
            }
        }

        // LKW beladen
        public void LKWbeladenNachKG()
        {
            lkwList.OrderByDescending(lkw => lkw.Nutzlast);
            palettenList.OrderByDescending(p => p.Gewicht);

            List<string> ladeListe = new List<string>();

            List<string> freieLKWListe = new List<string>();

            if (lkwList != null && palettenList != null)
            {

                foreach (var einLkw in lkwList)
                {
                    foreach (var einePalette in palettenList)
                    {
                        if (einLkw.Nutzlast > einePalette.Gewicht)
                        {
                            ladeListe.Add(string.Format("{0},{1},{2}", einLkw.Kennzeichen, einePalette.PalettenNr, einePalette.Gewicht));                            
                        }
                    }
                }
                foreach (var lkw in lkwList)
                {
                    if (!ladeListe.Contains(lkw.Kennzeichen))
                    {
                        freieLKWListe.Add(lkw.Kennzeichen);
                    }
                }
            }
            WriteTxt(ladeListe, freieLKWListe);
        }

        public void LKWbeladenNachPaletten()
        {

        }

        // Textdateien erstellen
        private void WriteTxt(List<string> ladeListe, List<string> freieLKW)
        {
            FileStream ladeListeDatei = new FileStream(@"D:\Neuer Ordner\LadeListe.txt", FileMode.OpenOrCreate);
            FileStream freieLKWDatei = new FileStream(@"D:\Neuer Ordner\FreieLKW.txt", FileMode.OpenOrCreate);

            using (StreamWriter ladeListeWriter = new StreamWriter(ladeListeDatei))
            {
                foreach (var item in ladeListe)
                {
                    ladeListeWriter.WriteLine(item);
                }
            }
            ladeListeDatei.Close();

            using (StreamWriter freieLKWListeWriter = new StreamWriter(freieLKWDatei))
            {
                foreach (var item in ladeListe)
                {
                    freieLKWListeWriter.WriteLine(item);
                }
            }
            freieLKWDatei.Close();
        }

        // Repository Inhalt auf Konsole ausgeben
        public void ToConsole()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("LKW Liste:");
            Console.ForegroundColor = ConsoleColor.Yellow;

            foreach (var item in lkwList)
            {
                Console.WriteLine(item.ToString());

            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Paletten Liste:");
            Console.ForegroundColor = ConsoleColor.Yellow;

            foreach (var item in palettenList)
            {
                Console.WriteLine(item.ToString());

            }

            Console.ResetColor();
        }
    }
}
