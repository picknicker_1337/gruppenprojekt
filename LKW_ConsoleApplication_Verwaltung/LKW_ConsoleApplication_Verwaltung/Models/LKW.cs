﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKW_ConsoleApplication_Verwaltung.Models
{
    class LKW
    {
        public string Kennzeichen { get; set; }
        public int Nutzlast { get; set; }
        public int MaxAnzahlPaletten { get; set; }
        
        public LKW (string Kennzeichen, int Nutzlast, int MaxAnzahlPaletten)
        {
            this.Kennzeichen = Kennzeichen;
            this.Nutzlast = Nutzlast;
            this.MaxAnzahlPaletten = MaxAnzahlPaletten;
        }
    }
}
