﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKW_ConsoleApplication_Verwaltung.Models
{
    class Palette
    {
        public string PalettenNr { get; set; }
        public int Gewicht { get; set; }

        public Palette(string PalettenNr, int Gewicht)
        {
            this.PalettenNr = PalettenNr;
            this.Gewicht = Gewicht;
        }
    }
}
